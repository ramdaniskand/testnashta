package id.co.test.nilai.controller;


import io.swagger.v3.oas.annotations.tags.Tag;
import net.sf.json.JSONObject;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import id.co.test.nilai.model.DataNilai;
import id.co.test.nilai.model.GeneralResponse;
import id.co.test.nilai.repository.DataNilaiRepository;
import id.co.test.nilai.repository.MahasiswaRepository;
import id.co.test.nilai.repository.MataPelajaranRepository;
import id.co.test.nilai.services.DataMahasiswaServices;

import java.util.ArrayList;
import java.util.List;


@Tag(name = "Data Mahasiswa API", 
	description = "Provides Data Mahasiswa API's.")
@RestController
@RequestMapping(path = "/nilai")
public class DataMahasiswaController {
	@Autowired
	DataNilaiRepository dataNilaiRepository;
	
	@Autowired
	MahasiswaRepository mahasiswaRepository;
	
	@Autowired
	MataPelajaranRepository mataPelajaranRepository;
	
	@Autowired
	DataMahasiswaServices dataMahasiswaServices;
	
	@PostMapping
	public GeneralResponse simpanData(@RequestBody DataNilai dataNilai) throws Exception {
		GeneralResponse listData = dataMahasiswaServices.createData(dataNilai);
		return listData;
	}
	
	@PutMapping(value = "/{id}")
    public GeneralResponse updateData(
    		@PathVariable Long id, 
    		@RequestBody DataNilai dataNilai) throws Exception {
		GeneralResponse listData = dataMahasiswaServices.updateData(id, dataNilai);
		return listData;
    }
	
	 @DeleteMapping("/{id}")
	    private void hapusData(
	    		@PathVariable Long id){
			dataNilaiRepository.deleteById(id);
	    }
	 
	 @GetMapping(value = "/id")
	 public GeneralResponse getData1(Long id) {
		 GeneralResponse response = new GeneralResponse();
		 response = dataMahasiswaServices.getData1(id);
		return response;
		 
	 }
	 
	 @GetMapping(value = "/all")
	 public GeneralResponse getDataNilai() {
		 GeneralResponse response = new GeneralResponse();
		 response = dataMahasiswaServices.getDataNilai();
		return response;
	 }
	 
	 @GetMapping(value = "average")
	 public GeneralResponse getAverage() {
		 GeneralResponse response = new GeneralResponse();
		 response = dataMahasiswaServices.getAverage();
		return response;
	 }

}
	