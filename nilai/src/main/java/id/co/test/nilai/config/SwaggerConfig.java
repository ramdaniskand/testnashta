package id.co.test.nilai.config;

import io.swagger.v3.oas.models.Components;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.text.SimpleDateFormat;
import java.util.Calendar;

@Configuration
public class SwaggerConfig {

    @Value("${app.swagger.title}")
    private String title;

    @Value("${app.swagger.description}")
    private String description;

    @Bean
    public OpenAPI customOpenAPI() {
        return new OpenAPI()
                .components(new Components())
                .info(
                        new Info().title(title)
                        .description(description)
                        .version(new SimpleDateFormat("yyyy.MM.dd-HHmmss").format(Calendar.getInstance().getTime()))
                );
    }

}
