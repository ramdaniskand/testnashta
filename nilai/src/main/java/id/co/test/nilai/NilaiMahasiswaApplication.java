package id.co.test.nilai;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NilaiMahasiswaApplication {

	public static void main(String[] args) {
		SpringApplication.run(NilaiMahasiswaApplication.class, args);
	}

}
