package id.co.test.nilai.repository;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import id.co.test.nilai.model.DataNilai;

public interface DataNilaiRepository extends JpaRepository<DataNilai, Long>{

	@Query(value = "SELECT * FROM DATA_NILAI "
			+ "WHERE ID_MAHASISWA = :idMahasiswa", nativeQuery = true)
    public List<DataNilai> getDatabyidMhs(
            @Param("idMahasiswa") Long idMahasiswa
    );
}
