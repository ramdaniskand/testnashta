package id.co.test.nilai.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import id.co.test.nilai.model.MataPelajaran;

public interface MataPelajaranRepository extends JpaRepository<MataPelajaran, Long> {

}
