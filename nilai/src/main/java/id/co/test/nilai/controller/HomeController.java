package id.co.test.nilai.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.view.RedirectView;

@Tag(name = "Home", description = "Default")
@RestController
@RequestMapping(path = "")
public class HomeController {

    @GetMapping
    public RedirectView hello()
    {
        return new RedirectView("./swagger-ui.html");
    }

}