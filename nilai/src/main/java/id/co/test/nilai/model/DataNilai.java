package id.co.test.nilai.model;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "data_nilai")
public class DataNilai implements Serializable {
	
	private static final long serialVersionUID = 74683265812312L;

	@Id
	@Column(name = "id")
	private Long id;
	
	@Column(name = "id_mahasiswa")
	private Long idMahasiswa;
	
	@Column(name = "id_mata_pelajaran")
	private Long idMataPelajaran;
	
	@Column(name = "nilai")
	private Long nilai;
	
	@Column(name = "keterangan")
	private String keterangan;

	
	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getIdMahasiswa() {
		return idMahasiswa;
	}

	public void setIdMahasiswa(Long idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}

	public Long getIdMataPelajaran() {
		return idMataPelajaran;
	}

	public void setIdMataPelajaran(Long idMataPelajaran) {
		this.idMataPelajaran = idMataPelajaran;
	}

	public Long getNilai() {
		return nilai;
	}

	public void setNilai(Long nilai) {
		this.nilai = nilai;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataNilai other = (DataNilai) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataNilai [id=");
		builder.append(id);
		builder.append(", idMahasiswa=");
		builder.append(idMahasiswa);
		builder.append(", idMataPelajaran=");
		builder.append(idMataPelajaran);
		builder.append(", nilai=");
		builder.append(nilai);
		builder.append(", keterangan=");
		builder.append(keterangan);
		builder.append("]");
		return builder.toString();
	}

	
	
}
