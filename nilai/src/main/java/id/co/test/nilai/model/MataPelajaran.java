package id.co.test.nilai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "mata_pelajaran")
public class MataPelajaran implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 746832658743L;

	@Id
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "nama_mata_pelajaran")
	private String mataPelajaran;
	
	@Column(name = "id_mahasiswa")
	private String idMahasiswa;

	
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getMataPelajaran() {
		return mataPelajaran;
	}

	public void setMataPelajaran(String mataPelajaran) {
		this.mataPelajaran = mataPelajaran;
	}

	public String getIdMahasiswa() {
		return idMahasiswa;
	}

	public void setIdMahasiswa(String idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}

	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MataPelajaran other = (MataPelajaran) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("MataPelajaran [id=");
		builder.append(id);
		builder.append(", mataPelajaran=");
		builder.append(mataPelajaran);
		builder.append(", idMahasiswa=");
		builder.append(idMahasiswa);
		builder.append("]");
		return builder.toString();
	}
	
}
