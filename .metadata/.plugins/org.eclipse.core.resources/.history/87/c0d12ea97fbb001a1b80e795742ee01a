package id.co.test.nilai.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name = "data_nilai")
public class DataNilai implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 74683265812312L;

	@Column(name = "id_mahasiswa")
	private Long idMahasiswa;
	
	@Column(name = "id_mata_pelajaran")
	private Long idMataPelajaran;
	
	@Column(name = "nilai")
	private String nilai;
	
	@Column(name = "keterangan")
	private String keterangan;

	public Long getIdMahasiswa() {
		return idMahasiswa;
	}

	public void setIdMahasiswa(Long idMahasiswa) {
		this.idMahasiswa = idMahasiswa;
	}

	public Long getIdMataPelajaran() {
		return idMataPelajaran;
	}

	public void setIdMataPelajaran(Long idMataPelajaran) {
		this.idMataPelajaran = idMataPelajaran;
	}

	public String getNilai() {
		return nilai;
	}

	public void setNilai(String nilai) {
		this.nilai = nilai;
	}

	public String getKeterangan() {
		return keterangan;
	}

	public void setKeterangan(String keterangan) {
		this.keterangan = keterangan;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idMahasiswa == null) ? 0 : idMahasiswa.hashCode());
		result = prime * result + ((idMataPelajaran == null) ? 0 : idMataPelajaran.hashCode());
		result = prime * result + ((keterangan == null) ? 0 : keterangan.hashCode());
		result = prime * result + ((nilai == null) ? 0 : nilai.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DataNilai other = (DataNilai) obj;
		if (idMahasiswa == null) {
			if (other.idMahasiswa != null)
				return false;
		} else if (!idMahasiswa.equals(other.idMahasiswa))
			return false;
		if (idMataPelajaran == null) {
			if (other.idMataPelajaran != null)
				return false;
		} else if (!idMataPelajaran.equals(other.idMataPelajaran))
			return false;
		if (keterangan == null) {
			if (other.keterangan != null)
				return false;
		} else if (!keterangan.equals(other.keterangan))
			return false;
		if (nilai == null) {
			if (other.nilai != null)
				return false;
		} else if (!nilai.equals(other.nilai))
			return false;
		return true;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DataNilai [idMahasiswa=");
		builder.append(idMahasiswa);
		builder.append(", idMataPelajaran=");
		builder.append(idMataPelajaran);
		builder.append(", nilai=");
		builder.append(nilai);
		builder.append(", keterangan=");
		builder.append(keterangan);
		builder.append("]");
		return builder.toString();
	}
	
	
}
