package id.co.jasindo.statusservices.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;
import java.util.Objects;

@Entity
@Table(name = "status")
public class StatusModel implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 746832658743L;

	@Id
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "NO_DOKUMEN")
	private String noDokumen;
	
	@Column(name = "ASAL")
	private String asal;
	
	@Column(name = "STATUS")
	private Short status;
	
	@Column(name = "STATUS_ASAL")
	private Short status_asal;
	
	@Column(name = "CATATAN")
	private String catatan;
	
	@Column(name = "USER")
	private String user;
	
	@Column(name = "CREATED_AT")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date createdAt;

	@Column(name = "UPDATED_AT")
	@Temporal(TemporalType.TIMESTAMP)
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss")
	private Date updatedAt;
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNoDokumen() {
		return noDokumen;
	}

	public void setNoDokumen(String noDokumen) {
		this.noDokumen = noDokumen;
	}

	public String getAsal() {
		return asal;
	}

	public void setAsal(String asal) {
		this.asal = asal;
	}

	public Short getStatus() {
		return status;
	}

	public void setStatus(Short status) {
		this.status = status;
	}

	public Short getStatus_asal() {
		return status_asal;
	}

	public void setStatus_asal(Short status_asal) {
		this.status_asal = status_asal;
	}

	public String getCatatan() {
		return catatan;
	}

	public void setCatatan(String catatan) {
		this.catatan = catatan;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	
	@Override
	public int hashCode() {
		return Objects.hash(id);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Mahasiswa other = (Mahasiswa) obj;
		return Objects.equals(id, other.id);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Log [id=");
		builder.append(id);
		builder.append(", noDokumen=");
		builder.append(noDokumen);
		builder.append(", asal=");
		builder.append(asal);
		builder.append(", status=");
		builder.append(status);
		builder.append(", status_asal=");
		builder.append(status_asal);
		builder.append(", catatan=");
		builder.append(catatan);
		builder.append(", user=");
		builder.append(user);
		builder.append(", createdAt=");
		builder.append(createdAt);
		builder.append(", updatedAt=");
		builder.append(updatedAt);
		builder.append("]");
		return builder.toString();
	}


	
	
}
